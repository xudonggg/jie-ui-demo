
// 基于 sessionStorage 实现全局缓存

const prefix = 'j-platform-'

const GlobalStoreService = {
    get: (key) => {
        return JSON.parse(sessionStorage.getItem(prefix + key))
    },
    set: (key, value) => {
        sessionStorage.setItem(prefix + key, JSON.stringify(value))
    },
    remove: (key) => {
        sessionStorage.removeItem(prefix + key)
    }
}
export default GlobalStoreService

export function YMD(value) {
  let time = new Date(value)

  let y = time.getFullYear()
  let m = time.getMonth() + 1
  let d = time.getDate();
  if (m < 10) {
      m = '0' + m
  }
  if (d < 10) {
      d = '0' + d
  }
  let ymd = y + '-' + m + '-' + d
  return ymd
}

/*获取路径参数*/
export function urlParams() {
  var str = location.search.length > 0 ? location.search.substring(1) : "";
  var items = str.length ? str.split("&") : [];
  var args = {},
    item = null,
    name = null,
    value = null;
  for (let i = 0, len = items.length; i < len; i++) {
    item = items[i].split("=");
    name = decodeURIComponent(item[0]);
    value = decodeURIComponent(item[1]);
    if (name.length) {
      args[name] = value;
    }
  }
  return args;
}

// table 多选处理
export const  filterSeletion = (List, selection, row, key = 'personCode') => {
  const Arr = []
  let IdArr = []

  List.forEach(item => {
      Arr.push(item[key])
  })
  selection.forEach(item => {
      IdArr.push(item[key])
  })

  if (row && IdArr.indexOf(row[key]) <= -1) {
      List.forEach((item, i) => {
          if (item[key] == row[key]) {
              List.splice(i, 1)
          }
      })
      return List
  } else {
      selection.forEach(item => {
          if (Arr.indexOf(item[key]) < 0) {
              List.push(item)
              Arr.push(item[key])
          }
      })

      return List
  }
}

// 根据列的索引生成表格中每列数据的编号
export const generateTableDataIndex = (index, pager) => {
    const { pageSize, currentPage } = pager;
    return index + 1 + (pageSize * (currentPage - 1)) + ''; // ElTooltip 需要字符串
};

// 日期   // 格式化日期 前台传值方式  引用类.dateFormat(1402233166999,"yyyy-MM-dd hh:mm:ss")
export const dateFormat = (value, fmt) => {
    if (!!value == false) {
      return '--';
    }
    let getDate = new Date(Number.parseInt(value));
    let o = {
      'M+': getDate.getMonth() + 1,
      'd+': getDate.getDate(),
      'h+': getDate.getHours(),
      'm+': getDate.getMinutes(),
      's+': getDate.getSeconds(),
      'q+': Math.floor((getDate.getMonth() + 3) / 3),
      'S': getDate.getMilliseconds()
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (getDate.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (let k in o) {
      if (new RegExp('(' + k + ')').test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)));
      }
    }
    return fmt;
};

// 节流函数
export const Throttle = (fn, t) =>{
  let last;
  let timer;
  let interval = t || 1000;
  return function () {
      let args = arguments;
      let now = +new Date();
      if (last && now - last < interval) {
          clearTimeout(timer);
          timer = setTimeout(() => {
              last = now;
              fn.apply(this, args);
          }, interval);
      } else {
          last = now;
          fn.apply(this, args);
      }
  }
};

export const  toggleClass = (element, className) => {
    if (!element || !className) {
      return
    }
    let classString = element.className
    const nameIndex = classString.indexOf(className)
    if (nameIndex === -1) {
      classString += ' ' + className
    } else {
      classString =
        classString.substr(0, nameIndex) +
        classString.substr(nameIndex + className.length)
    }
    element.className = classString
  }


  // 过滤参数为空的时候不传对应的key值
export const filterQuery = (params) => {
  let empParams = {};
  for (let key in params) {
    if (![null, undefined, ''].includes(params[key])) {
      // null、undefined、''时过滤掉
      empParams[key] = params[key];
    }
  }
  return empParams;
};


/**
 * 主题色切换 async
 */
export const getElementStyle = async () => {
  return new Promise((resolve,reject) => {
  axios({
    method: 'get',
    url: './static/elementUI.css'
  }).then(res => {
    const search = window.location.search?Object.assign({},...window.location.search.substring(1).split("&").map(val => ({[val.split("=")[0]]:val.split("=")[1]}))):{};
    let style = document.createElement('link');
    style.rel = "stylesheet";
    style.type = "text/css";
    console.log(121212)
    if(search.contentColor) {

      let theme = getThemeCluster(search.contentColor);
      ['409EFF', '64,158,255', '#53a8ff', '#66b1ff', '#79bbff', '#8cc5ff', '#a0cfff', '#b3d8ff', '#c6e2ff', '#d9ecff', '#ecf5ff', '#3a8ee6','#4e84fe'].forEach((val,index) => {
        res.data = res.data.replace(new RegExp(val,'g'),theme[index]);
      });
    }
    style.href = window.URL.createObjectURL(new Blob([res.data]));
    document.head.append(style);

    resolve();
  }).catch(function (err) {
    reject(err);
  })
});
}

/**
* 获取主题色
* @param {string} theme [主题色]
*/
export const getThemeCluster = (theme) => {
  const tintColor = (color, tint) => {
      let red = parseInt(color.slice(0, 2), 16)
      let green = parseInt(color.slice(2, 4), 16)
      let blue = parseInt(color.slice(4, 6), 16)
      if (tint === 0) { // when primary color is in its rgb space
          return [red, green, blue].join(',')
      } else {
          red += Math.round(tint * (255 - red))
          green += Math.round(tint * (255 - green))
          blue += Math.round(tint * (255 - blue))
          red = red.toString(16)
          green = green.toString(16)
          blue = blue.toString(16)
          return `#${red}${green}${blue}`
      }
  }
  const shadeColor = (color, shade) => {
      let red = parseInt(color.slice(0, 2), 16)
      let green = parseInt(color.slice(2, 4), 16)
      let blue = parseInt(color.slice(4, 6), 16)
      red = Math.round((1 - shade) * red)
      green = Math.round((1 - shade) * green)
      blue = Math.round((1 - shade) * blue)
      red = red.toString(16)
      green = green.toString(16)
      blue = blue.toString(16)
      return `#${red}${green}${blue}`
  }
  const clusters = [theme]
  for (let i = 0; i <= 9; i++) {
      clusters.push(tintColor(theme, Number((i / 10).toFixed(2))))
  }
  clusters.push(shadeColor(theme, 0.1))
  return clusters
}

/*base64转blob*/
export function dataURLtoBlob(dataurl) {
  var arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
  while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
  }
  return new Blob([u8arr], {
      type: mime
  });
}

//树扁平化
export function getTreeArr(arr) {

  let Arr = [];
  for (let i = 0; i < arr.length; i++) {
      Arr.push(arr[i])

      if (arr[i].children && arr[i].children.length > 0) {
          let list = getTreeArr(arr[i].children)
          Arr = Arr.concat(list)
      }
  }

  return Arr
}

//树扁平化
export function getTreeArr2(arr) {

  let Arr = [];
  for (let i = 0; i < arr.length; i++) {
      Arr.push(arr[i])

      if (arr[i].childs && arr[i].childs.length > 0) {
          let list = getTreeArr2(arr[i].childs)
          Arr = Arr.concat(list)
      }
  }

  return Arr
}
//
export function getYesterday(){
  let today=new Date()
      today.setTime(new Date().getTime() - 24 * 60 * 60 * 1000);
  let yesterday = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
  return [yesterday + ' 00:00:00',yesterday + ' 23:59:59']
}