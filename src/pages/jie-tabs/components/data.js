export const prjStatisticList = [{
    subsystemName: '测试项目一',
    countNum: 10
},{
    subsystemName: '测试项目二',
    countNum: 11
},{
    subsystemName: '测试项三',
    countNum: 12
}]

export const transformData = (list, headerList) => {
    if(!list || list.length == 0) {
        return [];
    }
    for (let i = 0; i < list.length; i++) {
        let tItem = list[i];
        let feeTypes = tItem.feeTypes;
        let tmjFeeType = [];
        for (let j = 0; j < headerList.length; j++) {
            let hItem = headerList[j];
            let addItem = {
                id: hItem.id,
                feeName: hItem.feeName,
                actualFee: '0.00'
            };
            for (let m = 0; m < feeTypes.length; m++) {
                if (hItem.id == feeTypes[m].feeTypeId) {
                    addItem.actualFee = feeTypes[m].oweFeeTotal;
                    break;
                }
            }
            tmjFeeType.push(addItem);
        }
        tItem.feeTypes = tmjFeeType;
    }
    return list;
}