import jieTabs from './index.vue'
export default {
  install(Vue) {
    Vue.component("jieTabs", jieTabs)
  }
}