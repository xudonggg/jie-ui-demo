import jieCustomerHeader from './index.vue'
export default {
  install(Vue) {
    Vue.component("jieCustomerHeader", jieCustomerHeader);
  }
}