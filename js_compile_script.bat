@echo off


echo ========== start compile ==========
rd /s /q .\js_compile_build\sys

mkdir js_compile_build\sys
mkdir js_compile_build\doc
npm config set registry http://10.101.224.136:8081/repository/npm-group && npm install && npm run build &&mkdir js_compile_build\sys\wysf&&xcopy "wysf" "js_compile_build\sys\wysf" /E&&cd js_compile_build

@echo on
