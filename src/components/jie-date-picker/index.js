import jieDatePicker from './index.vue'
export default {
  install(Vue) {
    Vue.component("jieDatePicker", jieDatePicker);
  }
}