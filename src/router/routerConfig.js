'use strict';
export default [{
  path: '/',
  redirect: {
    name: 'index'
  }
},
{
  path: '/index',
  name: 'index',
  meta: {
    requiresAuth: false,
    functionName: 'index'
  },
  component: () => import('@/pages/index')
},
{
  path: '/jieDialog',
  name: 'jieDialog',
  meta: {
    requiresAuth: false,
    functionName: 'jieDialog'
  },
  component: () => import('@/pages/jie-dialog/index')
},
{
  path: '/jieTop',
  name: 'jieTop',
  meta: {
    requiresAuth: false,
    functionName: 'jieDialog'
  },
  component: () => import('@/pages/jie-top/index')
},
{
  path: '/jieTabs',
  name: 'jieTabs',
  meta: {
    requiresAuth: false,
    functionName: 'jieTabs'
  },
  component: () => import('@/pages/jie-tabs/index')
}
];