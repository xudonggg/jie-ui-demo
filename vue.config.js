const path = require('path')

function resolve(dir) {
    return path.join(__dirname, '.', dir)
}

module.exports = {
    publicPath: process.env.NODE_ENV === 'development' ? '/' : './', // 部署应用包时的基本 URL
    outputDir: 'jie-ui-demo',
    // 用于嵌套生成的静态资产（js，css，img，fonts）的目录。
    assetsDir: './',
    lintOnSave: false, // 如果你不需要使用eslint，把lintOnSave设为false即可
    productionSourceMap: false, // 设为false打包时不生成.map文件
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "./src/assets/css/style.scss";`,
            },
        },
    },
    devServer: {
        port: 8881,
        open: true,
        proxy: {
            // 用户信息获取以及组织树信息获取
            '/jscp-community': {
                target: 'http://218.17.23.118:8755',
                changeOrigin: true,
                pathRewrite: {
                    '^/jscp-community': '/jscp-community'
                }
            },
        }
    },
    //修改或新增html-webpack-plugin的值，在index.html里面能读取htmlWebpackPlugin.options.title
    chainWebpack: config => {
        config.entry.app = ['babel-polyfill', './src/main.js'];
        config.plugin('html')
            .tap(args => {
                args[0].title = "物业缴费";
                return args;
            });

        config.module
            .rule('svg')
            .exclude.add(resolve('src/icons'))
            .end()
        config.module
            .rule('icons')
            .test(/\.svg$/)
            .include.add(resolve('src/icons'))
            .end()
            .use('svg-sprite-loader')
            .loader('svg-sprite-loader')
            .options({
                symbolId: 'icon-[name]'
            })
            .end()
        // config.plugin('webpack-bundle-analyzer').use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin)
    }
}


