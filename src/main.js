import Vue from 'vue'
import App from './App.vue'
import router from './router';
import './element'
// 引入公共组件
import jieTitle from '@/components/jie-title/index.js';
Vue.use(jieTitle);
import jieTop from '@/components/jie-top/index.js';
Vue.use(jieTop);
import jieTabs from '@/components/jie-tabs/index.js';
Vue.use(jieTabs);
import jieDropDate from '@/components/jie-drop-date/index.js';
Vue.use(jieDropDate);
import jieAdvanceDialog from '@/components/jie-advance-dialog/index.js';
Vue.use(jieAdvanceDialog);
import jieDatePicker from '@/components/jie-date-picker/index.js';
Vue.use(jieDatePicker);
import jieCustomerHeader from '@/components/jie-customer-header/index.js';
Vue.use(jieCustomerHeader);
import jieDialog from '@/components/jie-dialog/index.js';
Vue.use(jieDialog);
import jieProject from '@/components/jie-project/index.js';
Vue.use(jieProject);

// 引入公共样式
import "@/assets/iconfont/iconfont.css"; 
import './assets/css/jie-components.scss';

import clickoutside from '@/directive/clickoutside'
clickoutside.install(Vue)

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
