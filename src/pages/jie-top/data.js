export const TABLE_HEADER_COLUMN = [{
    name: "房间名称",
    prop: 'subsystemName',
    index: 1,
    width: 120
}, {
    name: "客户名称",
    prop: 'customerName',
    index: 2,
    width: 120
}, {
    name: "面积（m²）",
    prop: 'buildArea',
    index: 3,
    width: 300
}
]

export const prjStatisticList = [{
    subsystemName: '测试项目',
    customerName: '客户名称',
    buildArea: 10
}]

export const chargeItemHeader = ['应收（元）', '减免（元）', '应缴（元）', '已收（元）', '退款（元）', '实收（元）', '欠款（元）', '收缴率（含减免）', '收缴率（不含减免）', '减免金额比（元）']

export const chargeItemList = ['area1', 'area2', 'area3', 'area4', 'area5', 'area6', 'area7', 'area8', 'area9', 'area10'];

export const chargeList = [{
    name: '测试项目',
    area: 10,
    area1: 10,
    area2: 10,
    area3: 10,
    area4: 10,
    area5: 10,
    area6: 10,
    area7: 10,
    area8: 10,
    area9: 10,
    area10: 10
}]

export const getTargetItemList = () => {
    let result = [];
    for (let i = 0; i < chargeList.length; i++) {
        let obj = {
            header: ['应收（元）', '减免（元）', '应缴（元）', '已收（元）', '退款（元）', '实收（元）', '欠款（元）', '收缴率（含减免）', '收缴率（不含减免）', '减免金额比（元）'],
            filterVal: [],
            item: chargeList[i]
        }
        for (let j = 0; j < chargeItemList.length; j++) {
            let filterVal = `${chargeItemList[j]}${i}`;
            chargeList[i][`${chargeItemList[j]}${i}`] = chargeList[i][`${chargeItemList[j]}`];
            obj.filterVal.push(filterVal);
        }
        result.push(obj);
    }
    return result;
}

export const getTargetList = () => {
    let result = [];
    for (let i = 0; i < prjStatisticList.length; i++) {
        let item = prjStatisticList[i];
        for (let j = 0; j < chargeList.length; j++) {
            for (let m = 0; m < chargeItemList.length; m++) {
                item[`${chargeItemList[m]}${j}`] = chargeList[j][`${chargeItemList[m]}`];
            }
        }
        result.push(item);
    }
    return result;
}