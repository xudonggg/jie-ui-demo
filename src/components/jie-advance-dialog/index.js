import jieAdvanceDialog from './index.vue'
export default {
  install(Vue) {
    Vue.component("jieAdvanceDialog", jieAdvanceDialog);
  }
}