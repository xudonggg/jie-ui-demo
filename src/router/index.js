import Vue from 'vue';
import VueRouter from 'vue-router';
import routerConfig from './routerConfig';
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css'// progress bar style

import {Message} from 'element-ui';
Vue.prototype.$message = Message;
Vue.use(VueRouter);
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}


const router = new VueRouter({
  routes: routerConfig
});

router.beforeEach((to, from, next) => {
  NProgress.start() // start progress bar
  if (to.meta.requiresAuth) {
      const currentUser = GlobalStoreService.get('currentUser')
      if (!!currentUser && !!currentUser.jsToken) { // validate jsToken
          next()
      } else { // no jsToken, go to fw err page
          next('/err')
          NProgress.done() // finish progress bar
      }
  } else {
    next()
  }

})

router.afterEach(() => {
  // console.log('[router.afterEach => 退出路由导航守卫 <=] ')
  NProgress.done() // finish progress bar
})
export default router;
