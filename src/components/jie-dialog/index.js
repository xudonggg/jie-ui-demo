import jieDialog from './index.vue'
export default {
  install(Vue) {
    Vue.component("jieDialog", jieDialog)
  }
}