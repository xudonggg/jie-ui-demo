/**
 * 
 * Create by huadeLiang, 2020.01.10
 *
 * 给树弹窗添加显示隐藏指令
 * used by add attribute `v-clickoutside="closeTree"` 
 * closeTree : 执行方法,隐藏树弹窗 如 closeTree(){this.showTree = false}
 */
export default {
    bind(el, binding, vnode) {
        function documentHandler(e) {
            if (el.contains(e.target)) {
                return false;
            }
            if (binding.expression) {
                binding.value(e)
            }
        }
        el._vueClickOutside_ = documentHandler;
        document.addEventListener('click', documentHandler);
    },
    unbind(el, binding) {
        document.removeEventListener('click', el._vueClickOutside_);
        delete el._vueClickOutside_;
    }
}