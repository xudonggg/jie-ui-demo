export const showList = [{
    name: "收缴率（含减免333333333333333333333333）",
    prop: 'area8',
    index: 0
}, {
    name: "收缴率（不含减免）",
    prop: 'area9',
    index: 1
}, {
    name: "减免金额比（元）",
    prop: 'area10',
    index: 2
}, {
    name: "测试四",
    prop: 'area8',
    index: 3
}, {
    name: "测试五",
    prop: 'area9',
    index: 4
}, {
    name: "测试六",
    prop: 'area10',
    index: 5
}, {
    name: "测试七",
    prop: 'area8',
    index: 6
}, {
    name: "测试八",
    prop: 'area9',
    index: 7
}, {
    name: "测试九",
    prop: 'area10',
    index: 8
}, {
    name: "测试十",
    prop: 'area8',
    index: 9
}, {
    name: "测试十一",
    prop: 'area9',
    index: 10
}, {
    name: "测试十二",
    prop: 'area10',
    index: 11
}];

export const hideList = [{
    name: "隐藏一",
    prop: 'area8',
    index: 1
}, {
    name: "隐藏二",
    prop: 'area9',
    index: 2
}, {
    name: "测试三",
    prop: 'area10',
    index: 3
}]